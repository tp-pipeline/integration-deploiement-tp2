 #!/bin/bash

# Definition of the variable name containing the version number
VERSION_VAR="VERSION"

# Check if the environment variable already exists
if [ -z "$(eval echo \$$VERSION_VAR)" ]; then
  # If the variable does not exist, initialize to 1.0.1
  eval export $VERSION_VAR=1.0.1
else
  # If the variable exists, increment the last component of the version number
  CURRENT_VERSION=$(eval echo \$$VERSION_VAR)
  MAJOR=$(echo $CURRENT_VERSION | cut -d'.' -f1)
  MINOR=$(echo $CURRENT_VERSION | cut -d'.' -f2)
  PATCH=$(echo $CURRENT_VERSION | cut -d'.' -f3)
  PATCH=$((PATCH + 1)) # Incrementing the patch
  eval export $VERSION_VAR="${MAJOR}.${MINOR}.${PATCH}"
fi

echo "New version number: $(eval echo \$$VERSION_VAR)"