Développeurs :
Hugo Raoult, Damien Forafo, Lucas Perez

Vue d'ensemble
Le TP se compose de trois étapes principales dans le pipeline CI/CD:

Build (Construction): Construit l'image Docker de l'application React.
Test: Exécute les tests de l'application.
Deploy (Déploiement): Prépare l'application pour le déploiement.
Variables
DOCKER_IMAGE_NAME: Nom de l'image Docker de l'application.
MAJOR_VERSION & MINOR_VERSION: Versions majeure et mineure de l'application, utilisées pour tagger l'image Docker.
before_script
Définit des actions préliminaires, comme l'affichage des variables d'environnement CI_COMMIT_TAG et CI_COMMIT_REF_NAME, et l'exportation de la version de l'application.

Étapes du Pipeline
Build
Utilise une image Docker et les services associés pour construire l'image de l'application.
Construit l'image Docker avec un tag spécifique basé sur la version.
Pousse l'image construite vers un registre Docker.
Si un tag de commit Git est présent, l'image est également taguée comme latest et poussée.
Test
Utilise une image Node.js pour exécuter les tests.
Installe les dépendances et exécute les tests de l'application.
Deploy
Contient des commandes placeholder pour le déploiement, exécutées uniquement sur la branche principale.
Dockerfile pour l'Application React
Le Dockerfile se compose de deux étapes:

Construire l'application:

Utilise une image Node.js comme environnement de construction.
Installe les dépendances et construit l'application React.
Servir l'application:

Utilise Nginx pour servir l'application construite.
Copie les fichiers construits dans le répertoire de serveur Nginx.
Expose le port 80 et démarre Nginx pour servir l'application.
